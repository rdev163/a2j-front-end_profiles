import { User } from './user';

export const USERS: User[] = [
	{	id: 1,
		fullName: "Billy Bob Thornton",
        firstName: "Billy",
        lastName: "Thornton",
        phone: "505-555-5555",
        profileComplete: true,
        type: "Represented",
		notes: "uc divorce"
	},
	{	id: 2,
		fullName: "Tom Cruise",
        firstName: "Tom",
        lastName: "Cruise",
        phone: "505-222-2222",
        profileComplete: true,
        type: "Represented",
		notes: "divorce"
	},
	{	id: 3,
		fullName: "Rudy Giuliani",
        firstName: "Rudious",
        lastName: "Giuliani",
        phone: "212-212-1111",
        profileComplete: true,
        type: "Represented",
		notes: "serial divorce"
	},
	{	id: 4,
		fullName: "Steven Seagal",
        firstName: "Stefanovich",
        lastName: "Sigal",
        phone: "404-4444-0404",
        profileComplete: true,
        type: "Represented",
		notes: "high misdemeanors"
	},
];