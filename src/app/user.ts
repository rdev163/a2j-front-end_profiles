export class User {
	id: number;
	fullName: string;
	firstName: string;
	lastName: string;
	phone: string;
	profileComplete: true;
	type: string;
	notes: string;
}