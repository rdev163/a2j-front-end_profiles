import { Component, OnInit } from '@angular/core';
import { AutobahnRoutingService } from '../services/autobahn-routing.service';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(
  	private _ars:AutobahnRoutingService
  ) { }

  ngOnInit() {
  	this._ars.RPCCall("aura.a2j.public", "user.registration", "registration",
  			{
  				username: "Test Person",
  				email: "test@Person.org",
  				role: 3,
  				password: "N1nechars!",
  			}
  		).subscribe(response => {console.log(response)})
  }

}
